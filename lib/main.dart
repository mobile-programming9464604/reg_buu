import 'package:flutter/material.dart';
import 'package:flutter_regbuu/HomeScreen.dart';

void main() {
  runApp(MaterialApp(
    title: "App",
    home: HomeScreen(),
    debugShowCheckedModeBanner: false,
  ));
}
