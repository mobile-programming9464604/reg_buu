import 'package:flutter/material.dart';
import 'package:flutter_regbuu/StudentScreen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF1EFE2),
          appBar: null,
          body: Center(
            child: Column(
              children: [
                SizedBox(
                  height: 27,
                ),
                Image.asset(
                  "assets/img/logo.png",
                  width: 232,
                ),
                SizedBox(
                  height: 12,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      width: 32,
                    ),
                    Image.network(
                      "https://cdn.discordapp.com/attachments/914729468315176971/1069944987028050010/image.png",
                      width: 122,
                    ),
                    SizedBox(
                      width: 19,
                    ),
                    Flexible(
                      child: Column(
                        children: const [
                          Text(
                            "ศักดิ์สิทธิ์ แหวนวงค์",
                            style: TextStyle(
                                color: Color(0xFF7A7A7A),
                                fontSize: 18,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "รหัสนิสิต : 62160303",
                            style: TextStyle(
                                color: Color(0xFF7A7A7A),
                                fontSize: 18,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "คณะวิทยาการสารสนเทศ",
                            style: TextStyle(
                                color: Color(0xFF7A7A7A),
                                fontSize: 18,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "สถานภาพ : กำลังศึกษา",
                            style: TextStyle(
                                color: Color(0xFF7A7A7A),
                                fontSize: 18,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "อ. ที่ปรึกษา : อาจารย์วรวิทย์ วีระพันธุ์",
                            style: TextStyle(
                                color: Color(0xFF7A7A7A),
                                fontSize: 18,
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8, right: 4),
                        child: ElevatedButton(
                          child: Text(
                            "ลงทะเบียน",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(108, 62),
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),
                            ),
                            primary: Color(0xFFBBD9EE),
                            onPrimary: Color(0xFF206BA4),
                          ),
                          onPressed: () => {print("fgfg")},
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4, right: 4),
                        child: ElevatedButton(
                          child: Text(
                            "ผลการลงทะเบียน",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(108, 62),
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),
                            ),
                            primary: Color(0xFFBBD9EE),
                            onPrimary: Color(0xFF206BA4),
                          ),
                          onPressed: () => {print("fgfg")},
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4, right: 8),
                        child: ElevatedButton(
                          child: Text(
                            "ประวัติ",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          style: ElevatedButton.styleFrom(
                            minimumSize: Size(108, 62),
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(15),
                              ),
                            ),
                            primary: Color(0xFFBBD9EE),
                            onPrimary: Color(0xFF206BA4),
                          ),
                          onPressed: () => {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => StudentScreen(),
                                ))
                          },
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xFF206BA4),
                      borderRadius: BorderRadius.circular(15)),
                  width: 348,
                  height: 195,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text("ผลการศึกษา",
                          style: TextStyle(
                              color: Color(0xFFE7E4D3),
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xFFE7E4D3),
                                borderRadius: BorderRadius.circular(15)),
                            width: 140,
                            height: 62,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "C.Register",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "135",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 30,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xFFE7E4D3),
                                borderRadius: BorderRadius.circular(15)),
                            width: 140,
                            height: 62,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "C.Earn",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "120",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xFFE7E4D3),
                                borderRadius: BorderRadius.circular(15)),
                            width: 76,
                            height: 62,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "CA",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "123",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 38,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xFFE7E4D3),
                                borderRadius: BorderRadius.circular(15)),
                            width: 76,
                            height: 62,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "GP",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "319",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 38,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Color(0xFFE7E4D3),
                                borderRadius: BorderRadius.circular(15)),
                            width: 76,
                            height: 62,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "GPA",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                  "2.59",
                                  style: TextStyle(
                                      color: Color(0xFF206BA4), fontSize: 18),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 18,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 21,
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Color(0xFFE7E4D3),
                          borderRadius: BorderRadius.circular(15)),
                      width: 144,
                      height: 64,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "วิชาทั้งหมดที่เรียน",
                            style: TextStyle(
                                color: Color(0xFF206BA4), fontSize: 18),
                          ),
                          Text(
                            "48",
                            style: TextStyle(
                                color: Color(0xFF206BA4), fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 54,
                    ),
                    ElevatedButton(
                      child: Text(
                        "ตารางเรียน/สอบ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(108, 62),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(15),
                          ),
                        ),
                        primary: Color(0xFFBBD9EE),
                        onPrimary: Color(0xFF206BA4),
                      ),
                      onPressed: () => {print("fgfg")},
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
