import 'package:flutter/material.dart';

class StudentScreen extends StatelessWidget {
  const StudentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xFFF1EFE2),
          appBar: null,
          body: Center(
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Image.asset(
                  "assets/img/logo.png",
                  width: 232,
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFF206BA4),
                    borderRadius: BorderRadius.circular(25),
                  ),
                  width: 192,
                  height: 32,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        "ประวัตินิสิต",
                        style: TextStyle(
                            color: Color(0xFFE7E4D3),
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    Image.network(
                      "https://cdn.discordapp.com/attachments/914729468315176971/1069944987028050010/image.png",
                      width: 122,
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 9,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFBBD9EE),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 158,
                      height: 32,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 4,
                          ),
                          Text("ข้อมูลด้านการศึกษา",
                              style: TextStyle(
                                  color: Color(0xFF206BA4),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16))
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "รหัสนิสิต : 62160303",
                      style: TextStyle(
                        color: Color(0xFF7A7A7A),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "เลขที่บัตรประชาชน : 1730701015588",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "ชื่อ : นายศักดิ์สิทธิ์ แหวนวงค์",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "ชื่ออังกฤษ : MR. SAKSIT WAENWONG",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "คณะ : คณะวิทยาการสารสนเทศ",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "วิทยาเขต : บางแสน",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "วิธีรับเข้า : Admission กลาง",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "อ. ที่ปรึกษา : อาจารย์วรวิทย์ วีระพันธุ์",
                      style: TextStyle(color: Color(0xFF7A7A7A)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 9,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFBBD9EE),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      width: 158,
                      height: 32,
                      child: Column(
                        children: [
                          SizedBox(
                            height: 4,
                          ),
                          Text("ข้อมูลส่วนบุคคล",
                              style: TextStyle(
                                  color: Color(0xFF206BA4),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16))
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "สัญชาติ : ไทย",
                      style: TextStyle(
                        color: Color(0xFF7A7A7A),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "ศาสนา : พุทธ",
                      style: TextStyle(
                        color: Color(0xFF7A7A7A),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "หมู่เลือด : A",
                      style: TextStyle(
                        color: Color(0xFF7A7A7A),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
